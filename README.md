# sqa-test-task

## Test Task

![img.png](img.png)

# Implementation Details

1. Test suite is created using JUnit 5, Java and Rest Assured.
2. **tests** package contains the test calls for all the API calls
   1. GetTestParamaterized - implements the GetTest tests with parameterization option as alternate
3. **enums** package contains the list of status codes that is used for asserting each API call
4. **api** package contains the Rest Assured calls for making the API calls as needed
5. The results are generated as Allure reports
6. Parallel execution is implemented using junit properites file available inside **resources** folder

# Execution Details

1. To run the scenarios please use the command - mvn clean verify -PEverything (Additional profiles can be added to run individual tests as needed)
2. To view the reports please use the command - mvn allure:serve

# Sample Run reports

Without parallel execution at 16s - setting the thread count to 1 in pom.xml
![img_1.png](img_1.png)

Parallel execution at 7s - setting the thread count to 5 in pom.xml
![img_2.png](img_2.png)
